import { Connection, createConnection } from "typeorm";
import { User } from "./entity/User";
import { UserLogged } from "./entity/UserLogged";

export class Database {

    public static async createDbConnection() {
        await createConnection({
            type: "postgres",
            host: process.env.HOST,
            port: process.env.DB_PORT as unknown as number,
            username: process.env.DB_USER,
            password: process.env.PW,
            database: process.env.DB,
            entities: [
                User,
                UserLogged,
            ],
            synchronize: true,
            logging: false
        });
    }
}
