# Installation:
- First of all you need to install postgres and nodejs (I used nodejs v10.X.X) on linux server and you must have a copy of ".env.defult" file named ".env" on the project folder, You have to fill it with your information.
- Once nodejs had been installed go to the project folder and run `npm install`.

# Commands:
| Command | Description |
|---------|-------------|
| `npm run dev` | Start application on dev enviroment |
| `npm start` | Start application |
| `npm run *scriptName*` | Run package.json scripts |

# Resources
## *Database*
[TypeORM]( https://github.com/typeorm/typeorm): ORM (MySQL/MariaDB, PostgreSQL, SQLite..) for TypeScript and JavaScript that can be run in NodeJS, Cordova, Ionic...

[ReflectMetadata](https://www.npmjs.com/package/reflect-metadata): Used to add metadata to a type.. Like Java or C# with the annotations or atributes (dependency of TypeORM).

## *Used Middlewares*
|  | Description |
|:-----|-----|
| [morgan](https://github.com/expressjs/morgan) | Http request logger
| [error-handler](https://github.com/expressjs/errorhandler) | Gives information about errors

## *Others*
### Used
|  | Description |
|:-|-------------|
| [Guide](https://developer.okta.com/blog/2018/11/15/node-express-typescript) | Guide to use typescript with node and express
| [tslint](https://www.npmjs.com/package/tslint) | Analysis tool to alert the potential problems in your code. [config rules](https://palantir.github.io/tslint/rules/)
| [npm-run-all](https://www.npmjs.com/package/npm-run-all) | Used to run multiple npm scripts
| [nodemon](https://www.npmjs.com/package/nodemon) | Watch for changes on your files andd restarts the app
| [rimraf](https://www.npmjs.com/package/rimraf) | Used to recursively remove folders
| [dotenv](https://www.npmjs.com/package/dotenv) | Loads the enviroment variables from .env file into process.env object.
| [Active Record vs Data Mapper](https://github.com/typeorm/typeorm/blob/master/docs/active-record-data-mapper.md) |  Explains what are these patterns and their differences

### Not Used
[shelljs](https://www.npmjs.com/package/shelljs) can be used to execute shell commands.

[fs-extra](https://www.npmjs.com/package/fs-extra) extends the node fs module with features such as reading and writing JSON files.

[ts-node](https://www.npmjs.com/package/ts-node) used to run TypeScript file directly

[body-parser](https://github.com/expressjs/body-parser) parses the request bodies in req.body property

[cookie-parser](https://github.com/expressjs/cookie-parser) parses the cookie header in req.cookies
