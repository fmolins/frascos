export class FrascosResponse {
    private success: boolean;
    private message: string;
    private status: number;
    private data: any;

    constructor(status: number, message: string, data?: any) {
        if (status !== 200) {
            this.success = false;
        } else {
            this.success = true;
        }
        this.status = status;
        this.message = message;
        this.data = data;
    }
}
