import { NextFunction, Request, Response, Router } from "express";
import errorCodes from "../codes/errorCodes.json";
import { FrascosError } from "../FrascosError";

/**
 * /
 *
 * @class User
 */
export class Middlewares {

  /**
   * Create the routes.
   *
   * @class IndexRoute
   * @method create
   * @static
   */
    public static replaceUrl(request: Request, response: Response, next: NextFunction) {
        if (request.url.includes(Middlewares.ROOT_URL)) {
            request.url = request.url.replace(Middlewares.ROOT_URL, "");
            next();
        } else {
            next(new FrascosError(errorCodes.INVALID_URL));
        }
    }

    private static readonly ROOT_URL = "/api/v1";
}
