import { NextFunction, Request, Response, Router } from "express";
import { UserController } from "../controllers/UserController";
import { User } from "../entity/User";
import { FrascosError } from "../FrascosError";
import { FrascosResponse } from "../responses/Response";

/**
 * / route
 *
 * @class User
 */
export class Routes {

    private userController: UserController;

    constructor() {
        this.userController = new UserController();
    }

  /**
   * Create the routes.
   *
   * @class IndexRoute
   * @method create
   * @static
   */
    public create(router: Router) {
        // router.get("/", (req: Request, res: Response, next: NextFunction) => {
        //     // example how to save AR entity
        //     const user = new User();
        //     user.firstName = "Cesc";
        //     user.lastName = "Saw";
        //     user.username = "cesc";
        //     user.password = "Demo123456";
        //     user.save();
        //     res.send("<h1>Hello World!</h1>");
        // });
        this.login(router);
    }

    private login(router: Router) {
        router.post("/login", (req: Request, res: Response, next: NextFunction) => {
            const user = new User(req.body.username, req.body.password);
            this.userController.login(user).then((dbUser: User) => {
                res.send(new FrascosResponse(200, "", dbUser));
            }).catch((err: FrascosError) => {
                res.status(err.getStatus()).send(new FrascosResponse(err.getStatus(), err.getErrorKey()));
            });

        });

        router.put("/register", (req: Request, res: Response, next: NextFunction) => {
            const user = new User(req.body.username, req.body.password, req.body.firstName, req.body.lastName);
            this.userController.register(user, req.body.repeatPassword).then((userDb: User) => {
                res.send(new FrascosResponse(200, "", userDb));
            }).catch((err: FrascosError) => {
                res.status(err.getStatus()).send(new FrascosResponse(err.getStatus(), err.getErrorKey()));
            });

        });
    }
}
