import { QueryFailedError } from "typeorm";
import errorCodes from "./codes/errorCodes.json";

export class FrascosError extends Error {

    private status: number;
    private errorKey: string;
    private errorCode: number;

    constructor(errorCode: number, err?: Error) {
        super();
        switch (errorCode) {
            case errorCodes.INTERNAL_SERVER_ERROR:
                this.status = 500;
                this.errorCode = errorCodes.INTERNAL_SERVER_ERROR;
                this.errorKey = errorCode + "_INTERNAL_SERVER_ERROR";
                FrascosError.toConsole(err);
                break;
            case errorCodes.INVALID_URL:
                this.status = 400;
                this.errorCode = errorCodes.INVALID_URL;
                this.errorKey = "INVALID_URL";
                break;
            case errorCodes.PASSWORDS_ARE_NOT_THE_SAME:
                this.status = 422;
                this.errorCode = errorCodes.PASSWORDS_ARE_NOT_THE_SAME;
                this.errorKey = errorCode + "_PASSWORDS_ARE_NOT_THE_SAME";
                break;
            case errorCodes.USER_NOT_FOUND:
                this.status = 404;
                this.errorCode = errorCodes.USER_NOT_FOUND;
                this.errorKey = errorCode + "_USER_NOT_FOUND";
                break;
            case errorCodes.INVALID_USER_OR_PASSWORD:
                this.status = 404;
                this.errorCode = errorCodes.USER_NOT_FOUND;
                this.errorKey = errorCode + "_USER_NOT_FOUND";
                break;
            case errorCodes.USER_ALREADY_EXISTS:
                this.status = 409;
                this.errorCode = errorCodes.USER_ALREADY_EXISTS;
                this.errorKey = errorCode + "_USER_ALREADY_EXISTS";
                break;
        }
    }

    private static toConsole(err: Error) {
        let message = "Error: " + err.stack;
        if (err instanceof QueryFailedError) {
            const jsonErr = JSON.parse(JSON.stringify(err));
            message = jsonErr.severity + ": " + err.stack;
            message += "\n\n" + jsonErr.query + "\n";
        }
        // tslint:disable-next-line:no-console
        console.error(message);

    }

    public getStatus() {
        return this.status;
    }

    public getErrorCode() {
        return this.errorCode;
    }

    public getErrorKey() {
        return this.errorKey;
    }

}
