import bcrypt from "bcrypt";
import { NextFunction } from "express";
import errorCodes from "../codes/errorCodes.json";
import { User } from "../entity/User";
import { UserLogged } from "../entity/UserLogged";
import { FrascosError } from "../FrascosError";

export class UserController {

    private static readonly saltRounds = 10;

    public login(user: User) {
        return new Promise((resolve, reject) => {
            User.findOne({ username: user.username }).then((dbUser: User) => {
                if (!dbUser) {
                    return reject(new FrascosError(errorCodes.USER_NOT_FOUND));
                }
                this.compareHash(user.password + user.username, dbUser.password).then((res: string) => {
                    if (res) {
                        const userLogged = new UserLogged(dbUser);
                        userLogged.save().then((userLoggedDb: UserLogged) => {
                            dbUser.token = userLoggedDb.getToken();
                            return resolve(dbUser);
                        }).catch((err: Error) => {
                            return reject(new FrascosError(errorCodes.INTERNAL_SERVER_ERROR, err));
                        });
                    } else {
                        return reject(new FrascosError(errorCodes.INVALID_USER_OR_PASSWORD));
                    }
                });
            }).catch((err: Error) => {
                return reject(new FrascosError(errorCodes.INTERNAL_SERVER_ERROR, err));
            });
        });
    }

    public register(user: User, repeatPassword: string) {
        return new Promise((resolve, reject) => {
            if (user.password !== repeatPassword) {
                return reject(new FrascosError(errorCodes.PASSWORDS_ARE_NOT_THE_SAME));
            }
            User.findOne({username: user.username}).then((checkUser) => {
                if (checkUser) {
                    return reject(new FrascosError(errorCodes.USER_ALREADY_EXISTS));
                } else {
                    this.getHash(user.password + user.username).then((hash: string) => {
                        user.password = hash;
                        user.save().then((userDb: User) => {
                            return resolve(userDb);
                        }).catch((err: Error) => {
                            return reject(new FrascosError(errorCodes.INTERNAL_SERVER_ERROR, err));
                        });
                    }).catch((err: Error) => {
                        return reject(new FrascosError(errorCodes.INTERNAL_SERVER_ERROR, err));
                    });
                }
            }).catch((err: Error) => {
                return reject(new FrascosError(errorCodes.INTERNAL_SERVER_ERROR, err));
            });
        });
    }

    private getHash(text: string) {
        return new Promise((resolve, reject) => {
            bcrypt.hash(text, UserController.saltRounds, (err, hash) => {
                if (err) {
                    return reject(new FrascosError(errorCodes.INTERNAL_SERVER_ERROR, err));
                }
                return resolve(hash);
            });
        });
    }

    private compareHash(text: string, compareText: string) {
        return new Promise((resolve, reject) => {
            bcrypt.compare(text, compareText, (err, res) => {
                if (err) {
                    return reject(new FrascosError(errorCodes.INTERNAL_SERVER_ERROR, err));
                }
                return resolve(res);
            });
        });
    }

}
