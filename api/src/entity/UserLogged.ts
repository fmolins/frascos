import { BaseEntity, Column, Entity, Index, JoinColumn, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { User } from "./User";

@Entity()
export class UserLogged extends BaseEntity {

    @PrimaryGeneratedColumn()
    private id: number;

    @OneToOne((type) => User)
    @JoinColumn()
    private user: User;

    @Column()
    @Index({ unique: true })
    private token: string;

    @Column({type: "bigint"})
    private loginDate: number;

    private randtoken = require("rand-token");

    constructor(user: User) {
        super();
        this.user = user;
        this.token = this.randtoken.generate(16);
        this.loginDate = Date.now();
    }

    public getToken() {
        return this.token;
    }
}
