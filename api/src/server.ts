import bodyparser from "body-parser";
import dotenv from "dotenv";
import errorhandler from "errorhandler";
import express from "express";
import { NextFunction, Request, Response, Router } from "express";
import logger from "morgan";
import path from "path";
import "reflect-metadata";
import { Database } from "./db";
import { Middlewares } from "./middlewares/Middlewares";
import { Routes } from "./routes/Routes";

/**
 * The server.
 *
 * @class Server
 */
class Server {

    private app: express.Application;

    /**
     * Constructor.
     *
     * @class Server
     * @constructor
     */
    constructor() {
        this.app = express();

        // initialize configuration
        dotenv.config();

        // create expressjs application
        this.app = express();

        // use of the middlewares
        this.app.use(Middlewares.replaceUrl);
        this.app.use(bodyparser.json());

        // configure application
        this.config();

        // add routes
        this.routes();

        // start the express server
        this.app.listen( process.env.SERVER_PORT, () => {
            // tslint:disable-next-line:no-console
            console.log( `server started at http://localhost:${ process.env.SERVER_PORT }` );
        } );
    }

    /**
     * Configure application
     *
     * @class Server
     * @method config
     */
    private async config() {
        this.app.use(logger("dev"));
        await Database.createDbConnection();
        // error handling
        if (process.env.NODE_ENV === "development") {
            // tslint:disable-next-line:no-console
            console.log("development eviroment");
            this.app.use(errorhandler());
        }
    }

    /**
     * Create router
     *
     * @class Server
     * @method routes
     */
    private routes() {
        let router: express.Router;
        const routes = new Routes();
        router = express.Router();
        routes.create(router);
        this.app.use(router);
    }
}
export default new Server();
