* npm scripts can be very powerful and useful to any Node.js project. Scripts can be chained together in several ways. One way to chain scripts together is to use the pre and post prefixes. For example, if you have one script labeled start and another labeled prestart, executing npm run start at the terminal will first run prestart, and only after it successfully finishes does start run.

* Use "// tslint:disable-next-line:no-console" above the console.log lines, because tslint considers using console.log to be an issue for production code.

* DB config: If syncronize: false, the relations had to been created before each insert
