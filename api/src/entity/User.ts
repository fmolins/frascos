import {BaseEntity, Column, Entity, Index, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class User extends BaseEntity {

    @PrimaryGeneratedColumn()
    public id: number;

    @Column({nullable: true})
    public firstName: string;

    @Column({nullable: true})
    public lastName: string;

    @Column()
    @Index({ unique: true })
    public username: string;

    @Column({nullable: true})
    public password: string;

    public token: string;

    constructor()
    constructor(username: string, password: string)
    constructor(username: string, password: string, firstName: string, lastName: string)
    constructor(username?: string, password?: string, firstName?: string, lastName?: string) {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
    }

}
