import { Component, OnInit } from '@angular/core';
import { MatFormFieldModule } from '@angular/material';
import { FormBuilder, FormGroup, FormControl, Validators, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

    loginForm : FormGroup;

    constructor() {
        this.loginForm = new FormGroup({
            username : new FormControl(null,[Validators.required]),
            password : new FormControl(null,[Validators.required])
        });
    }

    ngOnInit() {
    }

}
